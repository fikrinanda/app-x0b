package nanda.fikri.app0b

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imageView ->{
                requestPermission()
            }
            R.id.btnInsert ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate ->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete ->{
                queryInsertUpdateDelete("delete")
            }
        }
    }

    var id : String? = ""
    var var1 : String = ""
    val url = "http://192.168.43.157/medsos/show_data.php"
    val url2 = "http://192.168.43.157/medsos/query_ins_upd_del.php"
    var imstr = ""
    var namafile = ""
    var fileuri = Uri.parse("")
    lateinit var mediaHelper : MediaHelper
    lateinit var dtAdapter :AdapterData
    var daftarDt = mutableListOf<HashMap<String,String>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        try{
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e : Exception){
            e.printStackTrace()
        }
        
        rg.setOnCheckedChangeListener { group, checkedId ->
            when(checkedId){
                R.id.rbSenang -> var1 = "Senang"
                R.id.rbSantai -> var1 = "Santai"
                R.id.rbSedih -> var1 = "Sedih"
                R.id.rbMarah -> var1 = "Marah"
            }
        }

        mediaHelper = MediaHelper()
        dtAdapter = AdapterData(daftarDt, this)
        list.layoutManager = LinearLayoutManager(this)
        list.adapter = dtAdapter
        imageView.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)

    }

    override fun onStart() {
        super.onStart()
        showData()
    }

    fun requestPermission() = runWithPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA){
        fileuri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileuri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK)
            if(requestCode == mediaHelper.getRcCamera()){
                imstr = mediaHelper.getBitmapToString(imageView,fileuri)
                namafile = mediaHelper.getMyFileName()
            }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val kode= jsonObject.getString("kode")
                if(kode.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    imageView.setImageResource(R.color.colorHolo)
                    edNama.setText("")
                    rg.clearCheck()
                    edCaption.setText("")
                    showData()
                }
                else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("nama",edNama.text.toString())
                        hm.put("perasaan",var1)
                        hm.put("caption",edCaption.text.toString())
                        hm.put("image",imstr)
                        hm.put("file",namafile)
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id",id.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("perasaan",var1)
                        hm.put("caption",edCaption.text.toString())
                        hm.put("image",imstr)
                        hm.put("file",namafile)
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id",id.toString())
                    }

                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showData(){
        val request = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarDt.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var dt = HashMap<String,String>()
                    dt.put("id",jsonObject.getString("id"))
                    dt.put("nama",jsonObject.getString("nama"))
                    dt.put("perasaan",jsonObject.getString("perasaan"))
                    dt.put("caption",jsonObject.getString("caption"))
                    dt.put("url",jsonObject.getString("url"))
                    daftarDt.add(dt)
                }
                dtAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


}